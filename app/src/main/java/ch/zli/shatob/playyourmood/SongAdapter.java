package ch.zli.shatob.playyourmood;

        import android.content.Context;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.LinearLayout;
        import android.widget.RelativeLayout;
        import android.widget.TextView;

        import java.util.ArrayList;

public class SongAdapter extends BaseAdapter {
    private ArrayList<Song> songs;
    private LayoutInflater songInflater;

    public SongAdapter(Context c, ArrayList<Song> songs){
        this.songs = songs;
        songInflater = LayoutInflater.from(c);
    }

    @Override
    public int getCount(){
        return songs.size();
    }

    @Override
    public Object getItem(int arg0){
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int arg0, View arg1, ViewGroup arg2){
        LinearLayout songLayout = (LinearLayout)songInflater.inflate(R.layout.song_entry, arg2, false);
        TextView songViewTitle = songLayout.findViewById(R.id.SongTitle);
        TextView songViewArtistYear = songLayout.findViewById(R.id.SongArtist);
        //TextView songViewGenre = songLayout.findViewById(R.id.SongGenre);
        Song actualSong = songs.get(arg0);
        songViewTitle.setText(actualSong.getTitle());
        songViewArtistYear.setText(actualSong.getArtist());
        //songViewGenre.setText(actualSong.getGenre());
        songLayout.setTag(arg0);
        return  songLayout;
    }
}
