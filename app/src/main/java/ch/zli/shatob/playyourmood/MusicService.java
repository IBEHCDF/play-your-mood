package ch.zli.shatob.playyourmood;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentUris;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class MusicService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener {
    private MediaPlayer mPlayer;
    private ArrayList<Song> songs;
    private int songPosition;
    private final IBinder musicBind = new MusicBinder();
    private String songTitle="";
    private static final int NOTIFY_ID=1;
    private boolean shuffle=false;
    private Random rand;

    @Override
    public IBinder onBind(Intent intent) {
        return musicBind;
    }

    @Override
    public boolean onUnbind(Intent intent){
        mPlayer.stop();
        mPlayer.release();
        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        if(mediaPlayer.getCurrentPosition()>0){
            mediaPlayer.reset();
            playNext();
        }
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        mediaPlayer.reset();
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
        Intent notIntent = new Intent(this, SongActivity.class);
        notIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendInt = PendingIntent.getActivity(this, 0,
                notIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(this);

        builder.setContentIntent(pendInt)
                .setSmallIcon(R.drawable.play)
                .setTicker(songTitle)
                .setOngoing(true)
                .setContentTitle("Playing").setContentText(songTitle);
        Notification not = builder.build();

        startForeground(NOTIFY_ID, not);
        SongActivity.showMusicController();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        songPosition=0;
        mPlayer = new MediaPlayer();
        initMusicPlayer();
        rand = new Random();
    }

    public void initMusicPlayer(){
        mPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mPlayer.setOnPreparedListener(this);
        mPlayer.setOnCompletionListener(this);
        mPlayer.setOnErrorListener(this);
    }

    public void setList(ArrayList<Song> theSongs){
        songs=theSongs;
    }

    public class MusicBinder extends Binder {
        MusicService getService() {
            return MusicService.this;
        }
    }

    public void playSong(){
        mPlayer.reset();
        Song songPlay = songs.get(songPosition);
        long currSong = songPlay.getId();
        Uri songUri = ContentUris.withAppendedId(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, currSong);
        try{
            mPlayer.setDataSource(getApplicationContext(), songUri);
        }
        catch(Exception e){
            Log.e("MUSIC SERVICE", "Error setting data source", e);
        }
        try {
            mPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        songTitle=songPlay.getTitle();
    }

    public void setSong(int songIndex){
        songPosition = songIndex;
    }
    public int getSongPosition(){
        return mPlayer.getCurrentPosition();
    }
    public int getDur(){
        return mPlayer.getDuration();
    }
    public boolean isPlaying(){
        return mPlayer.isPlaying();
    }
    public void pausePlayer(){
        mPlayer.pause();
    }
    public void seek(int position){
        mPlayer.seekTo(position);
    }
    public void go(){
        mPlayer.start();
    }
    public void playPrev(){
        songPosition--;
        if(songPosition<0) songPosition= songs.size()-1;
        playSong();
    }
    public void playNext(){
        if(shuffle){
            int newSong = songPosition;
            while(newSong==songPosition){
                newSong=rand.nextInt(songs.size());
            }
            songPosition=newSong;
        }
        else{
            songPosition++;
            if(songPosition>=songs.size()) songPosition=0;
        }
        playSong();
    }
    @Override
    public void onDestroy(){
        if (mPlayer != null) mPlayer.release();
        stopForeground(true);
    }

    public void setShuffle(){
        if (shuffle) shuffle=false;
        else shuffle=true;
    }
}
