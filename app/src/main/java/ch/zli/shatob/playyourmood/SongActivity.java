package ch.zli.shatob.playyourmood;

import android.Manifest;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.MediaController.MediaPlayerControl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import ch.zli.shatob.playyourmood.MusicService.MusicBinder;

public class SongActivity extends AppCompatActivity implements MediaPlayerControl {
    private ArrayList<Song> songList;
    private MusicService musicSrv;
    private Intent playIntent;
    private boolean musicBound = false;
    private static MusicController musicController;
    private boolean paused=false, playbackPaused=false;
    private ListView songListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        songListView = findViewById(R.id.SongListView);
        songList = new ArrayList<Song>();
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_GRANTED) {
            fillList();
            setMusicController();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] result) {
        if (requestCode == 1) {
            if (result.length > 0 && result[0] == PackageManager.PERMISSION_GRANTED) {
                fillList();
                setMusicController();
            } else {

            }
            return;
        }
    }
    @Override
    protected void onStart(){
        super.onStart();
        if (playIntent == null) {
            playIntent = new Intent(this, MusicService.class);
            bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
            startService(playIntent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }



    private ServiceConnection musicConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            MusicBinder binder = (MusicBinder) iBinder;
            musicSrv = binder.getService();
            musicSrv.setList(songList);
            musicBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            musicBound = false;
        }
    };

    public void findSongs() {
        ContentResolver musicResolver = getContentResolver();
        Uri musicUri =
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor songCursor =
                musicResolver.query(musicUri, null, null, null, null);
        Cursor genreCursor;
        if (songCursor != null && songCursor.moveToFirst()) {
            int idColumn = songCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int titleColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int artistColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            do {
                long actualID = songCursor.getLong(idColumn);
                String actualTitle = songCursor.getString(titleColumn);
                String actualArtist = songCursor.getString(artistColumn);
                String actualGenre = "";
                Uri uri =
                        MediaStore.Audio.Genres.getContentUriForAudioId("external", idColumn);
                genreCursor = getContentResolver().query(uri, null, null, null, null);
                int genre_column_index = genreCursor.getColumnIndexOrThrow(MediaStore.Audio.Genres.NAME);
                if (genreCursor != null && genreCursor.moveToFirst()) {
                    do {
                        actualGenre +=
                                genreCursor.getString(genre_column_index) + " ";
                    } while (genreCursor.moveToNext());
                }
                songList.add(new Song(actualID, actualTitle, actualArtist, actualGenre));
            } while (songCursor.moveToNext());
        }
    }

    public void fillList() {
        findSongs();
        Collections.sort(songList, new Comparator<Song>() {
            public int compare(Song a, Song b) {
                return a.getTitle().compareTo(b.getTitle());
            }
        });
        SongAdapter songAdapter = new SongAdapter(this, songList);
        songListView = findViewById(R.id.SongListView);
        songListView.setAdapter(songAdapter);
    }

    public void songClicked(View view) {
        musicSrv.setSong(Integer.parseInt(view.getTag().toString()));
        musicSrv.playSong();
        if(playbackPaused){
            //setMusicController();
            playbackPaused=false;
        }
        showMusicController();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_shuffle:
                musicSrv.setShuffle();
                break;
            case R.id.action_end:
                stopService(playIntent);
                musicSrv = null;
                System.exit(0);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        stopService(playIntent);
        musicSrv = null;
        unbindService(musicConnection);
        super.onDestroy();
    }

    @Override
    public void start() {
        musicSrv.go();
    }

    @Override
    public void pause() {
        playbackPaused=true;
        musicSrv.pausePlayer();
    }

    @Override
    public int getDuration() {
        if(musicSrv!=null && musicBound && musicSrv.isPlaying())
            return musicSrv.getDur();
        else{
                if(playbackPaused) return musicSrv.getDur();
            return 0;
        }
    }

    @Override
    public int getCurrentPosition() {
        if(musicSrv!=null && musicBound && musicSrv.isPlaying())
            return musicSrv.getSongPosition();
        else{
            if (playbackPaused) return musicSrv.getSongPosition();

            return 0;
        }
    }

    @Override
    public void seekTo(int i) {
        musicSrv.seek(i);
    }

    @Override
    public boolean isPlaying() {
        if(musicSrv!=null && musicBound)
            return musicSrv.isPlaying();
        return false;
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        return 0;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private void setMusicController(){
        musicController = new MusicController(this);
        musicController.setPrevNextListeners(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playNext();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playPrev();
            }
        });
        musicController.setMediaPlayer(this);
        musicController.setAnchorView(findViewById(R.id.SongListView));
        musicController.setEnabled(true);
    }
    private void playNext(){
        musicSrv.playNext();
        if(playbackPaused){
            setMusicController();
            playbackPaused=false;
        }
        showMusicController();
    }
    private void playPrev(){
        musicSrv.playPrev();
        if(playbackPaused){
            setMusicController();
            playbackPaused=false;
        }
        showMusicController();
    }
    @Override
    protected void onPause(){
        super.onPause();
        paused=true;
    }
    @Override
    protected void onResume(){
        super.onResume();
        if(paused){
            setMusicController();
            paused=false;
        }
    }
    @Override
    protected void onStop() {
        musicController.hide();
        super.onStop();
    }

    public static void showMusicController(){
        musicController.show(0);
    }
}
